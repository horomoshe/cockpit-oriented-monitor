const express = require('express');
const app = express();
const {valuesforMonitor,inputFunc} = require ("./input");

//Prevents bugs
app.all('*', function (req, res, next) {
    if (!req.get('Origin')) return next();
    res.set('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.set('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,x-api-key');
    next();
});

app.listen(3001, () => {
    inputFunc(valuesforMonitor);
    console.log('server ok');
});

app.get('/', (req, res) => {
    res.json(valuesforMonitor);
});

