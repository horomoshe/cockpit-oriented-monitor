
//npm i prompt-sync
const prompt = require("prompt-sync")({ sigint: true });

//object
exports.valuesforMonitor = {
    altitude: 0,
    his: 0,
    adi: 0
}

//Input function from the console
//Requires object receipt
exports.inputFunc =  (valuesforMonitor)=>{
    do{
        altiiude1 = Number(prompt("enter altitude "));
    }while((altiiude1<0)||(altiiude1>3000));
    do{
        his1 = Number(prompt("enter his "));
    }while((his1<0)||(his1>360));
    do{
        adi1 = Number(prompt("enter adi "));
    }while ((adi1<(-100))||(adi1>100));

    //Only after input confirmation, Enter data for an object
    //הכנסתי אחרי הלולאות כדי לא יכניס לאובייקט זבל אפילו לא לרגע
    valuesforMonitor.altitude = altiiude1;
    valuesforMonitor.his =his1;
    valuesforMonitor.adi = adi1;
}
