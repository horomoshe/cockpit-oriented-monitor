import React, { useEffect, useState } from 'react';
import axios from 'axios';

import MonitoriWindow from "./components/monitoriWindow";
import Buttons from "./components/buttons"
import MonitorText from "./components/monitorText";

import './App.css'; //צריך להתאים?



function App() {
  ////הוקים
  const [coordiantes, setCoordiantes] = useState([]);

  const [visual, setVisual] = useState({
    visual: true
  })

  //Object absorption from port 3001
  const doCoordiantes = async () => {
    let url = "http://localhost:3001/";
    await axios.get(url).then((res) => {
      setCoordiantes(res.data);
      console.log(coordiantes);
    })
  }

  useEffect(() => {
    doCoordiantes();
  },[])

  return (
    <div className="App">
      <div className="main" >
        <Buttons setVisual={setVisual} />
        {
          visual ? (
            <MonitoriWindow coordiantes={coordiantes} />) : (
            <MonitorText coordiantes={coordiantes} />)
        }
      </div>

    </div>
  )
}

export default App;
